﻿using System.Collections.Generic;

namespace YShop.Application.Contracts.Shop
{
    /// <summary>
    /// 商品选项
    /// </summary>
    public class ProductOptionDto:BaseEntityDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选项 
        /// </summary>
        public List<ProductOptionDetailDto> Items { get; set; }
        /// <summary>
        /// 选项连接
        /// </summary>
        public string ItemJoin { get; set; }
    }
    /// <summary>
    /// 商品选项子项
    /// </summary>
    public class ProductOptionDetailDto : BaseEntityDto
    {
        /// <summary>
        /// 选项ID
        /// </summary>
        public string ProductOptionId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 参数
        /// </summary>
        public string Params { get; set; }
    }
}
