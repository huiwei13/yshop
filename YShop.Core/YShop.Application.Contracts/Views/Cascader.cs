﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.Views
{
    /// <summary>
    /// 分层DTO
    /// </summary>
    public class Cascader
    {
        public Cascader()
        {
        }

        public Cascader(string id, string parentId, string label, dynamic value)
        {
            Id = id;
            ParentId = parentId;
            Label = label;
            Value = value;
        }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 父级
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 显示名
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public dynamic Value { get; set; }

        /// <summary>
        /// 子项
        /// </summary>
        public List<Cascader> Children { get; set; }
    }
}
