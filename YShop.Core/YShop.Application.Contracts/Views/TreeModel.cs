﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts
{
    /// <summary>
    /// 树模型
    /// </summary>
    public class TreeModel<TDto> : BaseEntityDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// 父级Id
        /// 最顶级的父Id为 '0'
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 左节点
        /// </summary>
        public int Lft { get; set; }
        /// <summary>
        /// 右节点
        /// </summary>
        public int Rgt { get; set; }
        /// <summary>
        /// 子项
        /// </summary>
        public List<TDto> Children { get; set; } = new List<TDto>();

    }
}
