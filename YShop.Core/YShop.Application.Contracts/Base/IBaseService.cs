﻿using Furion.DependencyInjection;
using Furion.DynamicApiController;

namespace YShop.Application
{
    /// <summary>
    /// 基类服务
    /// </summary>
    public interface IBaseService: IScoped, IDynamicApiController
    {
    }
}
