﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.System.Menus
{
    public class MenuHasChildrenDto:MenuDto
    {
        /// <summary>
        /// 子项
        /// </summary>
        public List<MenuHasChildrenDto> Children { get; set; }


        public void AddChildren(MenuHasChildrenDto childrenDto)
        {
            Children ??= new List<MenuHasChildrenDto>();
            Children.Add(childrenDto);
            HasChild = true;
        }
    }
}
