﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.System.DataItems
{
    public class DataItemCategoryDto : BaseEntityDto
    {
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 父级Id
        /// 最顶级的父Id为 '0'
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 左节点
        /// </summary>
        public int Lft { get; set; }
        /// <summary>
        /// 右节点
        /// </summary>
        public int Rgt { get; set; }
    }
}
