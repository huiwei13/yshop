﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.AgileDevelop.DatabaseManagement
{
    public class DbColumnInfoDto
    {
        public string Name { get; set; }
        public int DbType { get; set; }
        public string DbTypeText { get; set; }
        public string DbTypeTextFull { get; set; }
        public int MaxLength { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsIdentity { get; set; }
        public bool IsNullable { get; set; }
        public string Coment { get; set; }
        public string DefaultValue { get; set; }
        public int Position { get; set; }
    }
}
