﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;

namespace YShop.Application.Contracts.Caches
{
    public interface IMemoryCacheService
    {
        /// <summary>
        /// 清空缓存
        /// </summary>
        void Clear();
    }
}
