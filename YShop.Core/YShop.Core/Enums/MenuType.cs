﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core
{
    /// <summary>
    /// 菜单类型
    /// </summary>
    public enum MenuType
    {
        /// <summary>
        /// 根
        /// </summary>
        Root,
        /// <summary>
        /// 组
        /// </summary>
        Group,
        /// <summary>
        /// 视图
        /// </summary>
        View
    }
}
