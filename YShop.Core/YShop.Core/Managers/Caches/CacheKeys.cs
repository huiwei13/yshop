﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core.Managers.Caches
{
    public static class CacheKeys
    {
        public static string Menus => "_Menus";
        public static string DataItems => "_Data_Items";
        public static string DataItemCategorys => "_Data_Item_Categorys";
        public static string Routes => "_Routes";
        public static string ProductAttributes => "_ProductAttributes";
        public static string Products => "_Products";
        public static string ProductOptions => "_ProductOptions";

        public static string ProductCategroys => "_ProductCategroys";
    }
}
