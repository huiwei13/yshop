﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion.DependencyInjection;

namespace YShop.Core
{
    /// <summary>
    /// 只有ID的实体
    /// </summary>
    [SkipScan]
    public class OnlyPrimaryEntity : IBaseEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [MaxLength(36)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
    }
}
