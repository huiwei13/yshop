﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YShop.Core.Entites.Shop;

namespace YShop.Core.Entites
{
    /// <summary>
    /// 销售订单
    /// </summary>
    public class Order:BaseEntity, IAuditEntity
    {
        public Order()
        {
            AuditState = AuditState.Auditing;
        }

        /// <summary>
        /// 销售订单编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 来源（wechat、h5、web...）
        /// </summary>
        public string Source { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 客户
        /// </summary>
        public User User { get; set; }
        /// <summary>
        /// 审核人编号
        /// </summary>
        public string AuditUserCode { get; set; }
        /// <summary>
        /// 审核时间
        /// </summary>
        public DateTime? AuditTime { get; set; }
        /// <summary>
        /// 是否审核
        /// </summary>
        public bool IsAudit { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        public AuditState AuditState { get; set; }
        /// <summary>
        /// 订单明细
        /// </summary>
        public List<OrderDetail> Items { get; set; }
    }

    /// <summary>
    /// 销售单明细
    /// </summary>
    public class OrderDetail
    {
        /// <summary>
        /// 商品Id
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// SKU
        /// </summary>
        public string ProductDetailId { get; set; }
        public ProductDetail ProductDetail { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 发货单
        /// </summary>
        public string WarehouseAccessId { get; set; }
        /// <summary>
        /// 物流
        /// </summary>
        public string LogisticsId { get; set; }
    }
}
