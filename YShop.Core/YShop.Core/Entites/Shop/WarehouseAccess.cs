﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core.Entites
{
    /// <summary>
    /// 出入库
    /// </summary>
    public class WarehouseAccess: BaseEntity
    {
        /// <summary>
        /// 出库编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 出入库类型
        /// </summary>
        public WarehouseAccessType Type { get; set; }
        /// <summary>
        /// 仓库编号
        /// </summary>
        public string WarehouseCode { get; set; }
        /// <summary>
        /// 来源编号
        /// </summary>
        public string SourceCode { get; set; }
    }

    /// <summary>
    /// 出入库明细
    /// </summary>

    public class WarehouseAccessDetail : BaseEntity 
    {
        /// <summary>
        /// SKUId
        /// </summary>
        public string ProductDetailId { get; set; }
        /// <summary>
        /// SKU码
        /// </summary>
        public string ProductDetailCode { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }
        /// <summary>
        /// 库位
        /// </summary>
        public string Location { get; set; }
    }

    /// <summary>
    /// 出入库类型
    /// </summary>
    public enum WarehouseAccessType
    {
        PurchasingIn,
        SalesOut,
        SalesReturn
    }
}
