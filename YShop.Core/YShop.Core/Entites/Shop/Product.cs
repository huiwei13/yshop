﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YShop.Core.Entites.Shop
{
    /// <summary>
    /// 产品（SUP）
    /// </summary>
    public class Product: BaseEntity
    {
        /// <summary>
        /// 产品编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 子名称
        /// </summary>
        public string SubName { get; set; }
        /// <summary>
        /// 上架
        /// </summary>
        public bool Saleable { get; set; }
        /// <summary>
        /// 产品描述（富文本）
        /// </summary>
        [Column(TypeName = "text")]
        public string Describe { get; set; }
        /// <summary>
        /// 库存单位（SKU）
        /// </summary>
        public List<ProductDetail> Details { get; set; }
        /// <summary>
        /// 产品属性
        /// </summary>
        public List<ProductAttribute> Attributes { get; set; }
        /// <summary>
        /// 主图
        /// </summary>
        [Column(TypeName = "text")]
        public string Images { get; set; }
        /// <summary>
        /// 商品分类ID
        /// </summary>
        public string CategroyId { get; set; }
    }


    /// <summary>
    /// 产品明细（SKU）
    /// </summary>
    public class ProductDetail: BaseEntity
    {
        /// <summary>
        /// 产品ID
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// SKU编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 内部价
        /// </summary>
        public decimal InsidePrice { get; set; }
        /// <summary>
        /// 图片（如果没有，则默认使用主图）
        /// </summary>
        public string Images { get; set; }
        /// <summary>
        /// 选项
        /// </summary>
        [Column(TypeName = "text")]
        public string Options { get; set; }
    }

    /// <summary>
    /// 产品分类
    /// </summary>
    public class ProductCategroy : BaseEntity
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 产品分类名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string Picture { get; set; }

    }



}
