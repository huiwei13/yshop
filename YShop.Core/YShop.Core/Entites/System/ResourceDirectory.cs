﻿using System.ComponentModel.DataAnnotations.Schema;

namespace YShop.Core.Entites.System
{
    /// <summary>
    /// 资源目录
    /// </summary>
    [Table("Sys_ResourceDirectory")]
    public class ResourceDirectory : BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 公开
        /// </summary>
        public bool IsPublic { get; set; }
    }
}
