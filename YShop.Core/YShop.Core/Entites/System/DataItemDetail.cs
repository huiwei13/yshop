﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace YShop.Core.System
{
    /// <summary>
    /// 数据项明细
    /// </summary>
    [Table("Sys_DataItemDetail")]
    public class DataItemDetail : BaseEntity, IEntitySeedData<DataItemDetail>
    {
        /// <summary>
        /// 数据项Id
        /// </summary>
        public string DataItemId { get; set; }
        /// <summary>
        /// 显式名
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// 隐式值
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// 简称
        /// </summary>
        public string ShortName { get; set; }

        public IEnumerable<DataItemDetail> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<DataItemDetail>
            {
                // 菜单类型
                new DataItemDetail{ Id = "5ff4df67-2999-3150-0090-c5482ae88786",DataItemId = "5ff4ded3-2999-3150-0090-c54609c792e1",Label = "根菜单",Value = "0",ShortName = "GCD" },
                new DataItemDetail{ Id = "5ff4df71-2999-3150-0090-c5491c774728",DataItemId = "5ff4ded3-2999-3150-0090-c54609c792e1",Label = "二级菜单",Value = "1",ShortName = "EJCD" },
                new DataItemDetail{ Id = "5ff4dff2-2999-3150-0090-c54c769c38b5",DataItemId = "5ff4ded3-2999-3150-0090-c54609c792e1",Label = "视图菜单",Value = "2",ShortName = "STCD" },
                // 数据项类型
                new DataItemDetail{ Id = "5ff4df78-2999-3150-0090-c54a3d341ff1",DataItemId = "5ff4df02-2999-3150-0090-c54743ae8369",Label = "字典",Value = "0",ShortName = "ZD" },
                new DataItemDetail{ Id = "5ff4df7f-2999-3150-0090-c54b3b02b4e0",DataItemId = "5ff4df02-2999-3150-0090-c54743ae8369",Label = "数据源",Value = "1",ShortName = "SJY" },
            };
        }
    }
}
