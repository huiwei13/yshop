﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core.System
{
    /// <summary>
    /// 权限表
    /// </summary>
    public class Security : BaseEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public Security()
        {
        }

        /// <summary>
        /// 权限唯一名（每一个接口）
        /// </summary>
        [Required, MaxLength(100)]
        public string UniqueName { get; set; }

        /// <summary>
        /// 全新描述
        /// </summary>
        [Required, MaxLength(200)]
        public string Remark { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }

    }
}
