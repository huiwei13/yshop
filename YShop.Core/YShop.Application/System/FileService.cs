﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using YShop.Core;
using YShop.Core.System;

namespace YShop.Application.System
{
    /// <summary>
    /// 文件管理
    /// </summary>
    [ApiDescriptionSettings("System")]
    public class FileService : IBaseService
    {
        private readonly FileManager _fileManager;
        private readonly IConfiguration _configuration;

        public FileService(FileManager fileManager,IConfiguration configuration)
        {
            _fileManager = fileManager;
            _configuration = configuration;
        }

        /// <summary>
        /// 上传
        /// </summary>
        /// <param name="file"></param>
        /// <param name="directory"></param>
        /// <returns></returns>
        public async Task<Resource> Upload(IFormFile file, string directory)
        {
            return await _fileManager.UploadFile(file, directory);
        }
    }
}
