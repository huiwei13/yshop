﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YShop.Core;

namespace YShop.Application.System
{
    /// <summary>
    /// 开发服务
    /// </summary>
    [ApiDescriptionSettings("AgileDevelop")]
    public class DevelopService: IBaseService
    {
        private readonly DatabaseManager _databaseManager;

        public DevelopService(DatabaseManager databaseManager)
        {
            _databaseManager = databaseManager;
        }

        /// <summary>
        /// 获取有序的Guid
        /// </summary>
        /// <returns></returns>
        public string GetGuidAsync()
        {
            return Utils.NewGuid();
        }

        /// <summary>
        /// 同步数据库结构
        /// </summary>
        public void SyncDataStructure()
        {
            _databaseManager.AutoSyncDataStructure();
        }

        /// <summary>
        /// 重载种子数据
        /// </summary>
        public void ReloadSeedData()
        {
            _databaseManager.ReloadSeedData();
        }
    }
}
