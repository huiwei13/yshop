﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Excel.Contracts
{
    public class ExcelReadConfig
    {
        public string Path { get; set; }
        public string SheetName { get; set; }
        public int ColumnRowIndex { get; set; } = 0;
        //public Dictionary<string,string> ColumnForChineseMapper { get; set; }
    }
}
