﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Contracts.Excel
{
    public interface IExcelService
    {
        void Load(string path);

        void LoadSeed(string directory);
    }
}
