const state = {
	fold: true,
}
const mutations = {
	toggle:(state,isfold)=>{
		state.fold = isfold
	}
}
const actions = {
	fold:({ commit }) => {
		commit('toggle', true)
	},
	unfold:({ commit }) => {
		commit('toggle', false)
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}