/**
 * 商品属性
 */

import request from '@/utils/request'

/**
 * 获取商品属性
 * @param {any}} search 
 * @returns 
 */
 export function getList(search){
    return request({
        url: '/api/attribute/list',
        method: 'get',
        params:search
    })
}

/**
 * 获取属性下拉数据
 */
export function getAttributeItems(){
    return request({
        url: '/api/attribute/items',
        method: 'get',
        params:search
    })
}
/**
 * 根据Id获取商品属性
 * @param {string} id 
 * @returns 
 */
export function get(id){
    return request({
        url: '/api/attribute/' + id,
        method: 'get'
    })
}
/**
 * 更新商品属性
 * @param {object} update 
 * @returns 
 */
export function update(update){
    return request({
        url: '/api/attribute',
        method: 'put',
        data:update
    })
}
/**
 * 插入商品属性
 * @param {object} insert 
 * @returns 
 */
export function insert(insert){
    return request({
        url: '/api/attribute',
        method: 'post',
        data:insert
    })
}

export function remove(id){
    return request({
        url: '/api/attribute/' + id,
        method: 'delete',
    })
}