import request from '@/utils/request'

/**
 * 获取商品列表
 * @param {any} search 
 * @returns 
 */
export function getList(search){
    return request({
        url: '/api/product',
        method: 'get',
        params:search
    })
}

/**
 * 获取单个商品
 * @param {string} keyValue 
 */
export function get(keyValue){
    return request({
        url: '/api/product/' + keyValue,
        method: 'get',
    })
}
/**
 * 商品保存
 * @param {any} dto 
 * @returns 
 */
export function save(dto){
    return request({
        url: '/api/product/save-form',
        method: 'post',
        data:dto
    })
}

