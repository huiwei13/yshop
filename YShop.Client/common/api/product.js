import { request } from '../request.js'

export function getList(search){
	return request({
		url:'/api/product',
		method:'get',
		params:search
	})
}


export function get(id){
	return request({
		url:'/api/product/' + id,
		method:'get',
	})
}