import Vue from 'vue'

var _api = {}

const apiList = require.context('./api', true, /\.js$/)
apiList.keys().reduce((api, path) => {
    const apiCategoryName = path.replace(/^\.\/(.*)\.\w+$/, '$1')
    var apis = apiList(path)
	_api[apiCategoryName] = {}
    for (let f in apis) {
        _api[apiCategoryName][f] = apis[f]
    }
}, {})

Vue.prototype.api = _api;